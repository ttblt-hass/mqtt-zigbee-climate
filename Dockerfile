FROM python:3.7-slim-stretch

COPY requirements.txt /requirements.txt

RUN apt-get update -y && \
    apt-get install -y procps --no-install-recommends && \
	pip install -r /requirements.txt && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt


RUN mkdir -p /app
WORKDIR /app

COPY Dockerfile /Dockerfile
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY requirements.txt  setup.py  test_requirements.txt /app/
RUN pip install https://github.com/M-o-a-T/aioping/archive/v0.2.1.zip
RUN pip install -r /app/requirements.txt
COPY mqtt_climate_zigbee /app/mqtt_climate_zigbee

RUN python setup.py install

ENV DEVICE=/dev/ttyUSB1 
ENV DATABASE=/data/app.db
ENV BAUDRATE=57600
ENV MQTT_USERNAME=username
ENV MQTT_PASSWORD=password
ENV MQTT_HOST=192.168.0.1
ENV MQTT_PORT=1883
ENV ROOT_TOPIC=hass/zigbee
ENV CHANNEL=15
ENV LOG_LEVEL=INFO

CMD bash entrypoint.sh
