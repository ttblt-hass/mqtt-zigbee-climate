"""Setup file."""
import sys

from setuptools import setup
from mqtt_climate_zigbee.__main__ import VERSION


if sys.version_info < (3, 7):
    sys.exit('Sorry, Python < 3.7 is not supported')

install_requires = list(val.strip() for val in open('requirements.txt'))
tests_require = list(val.strip() for val in open('test_requirements.txt'))

setup(name='mqtt-climate-zigbee',
      version=VERSION,
      description=('Daemon managing Zigbee Climate with '
                   'Home-Assistant through MQTT'),
      author='Thibault Cohen',
      author_email='titilambert@gmail.com',
      url='http://gitlab.com/titilambert/mqtt_climate_zigbee',
      package_data={'': ['LICENSE.txt']},
      include_package_data=True,
      packages=['mqtt_climate_zigbee'],
      entry_points={
          'console_scripts': [
              'mqtt_climate_zigbee = mqtt_climate_zigbee.__main__:main',
              'mqtt_climate_zigbee_permit = mqtt_climate_zigbee.permit:permit'
          ]
      },
      license='Apache 2.0',
      install_requires=install_requires,
      tests_require=tests_require,
      classifiers=[
        'Programming Language :: Python :: 3.7',
      ]
      )
