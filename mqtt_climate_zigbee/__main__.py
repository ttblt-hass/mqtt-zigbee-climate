"""Module defining entrypoint."""
import asyncio

from mqtt_climate_zigbee import device

VERSION = "0.1.0"


def main():
    """Entrypoint function."""
    dev = device.MqttZigbeeClimate()
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
