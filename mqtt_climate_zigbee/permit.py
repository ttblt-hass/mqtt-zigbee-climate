"""Entrypoint for permit function."""
import os

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish


def permit():
    """Permit new device addition to the network."""
    mqtt_username = os.environ['MQTT_USERNAME']
    mqtt_password = os.environ['MQTT_PASSWORD']
    mqtt_host = os.environ['MQTT_HOST']
    try:
        mqtt_port = int(os.environ['MQTT_PORT'])
    except ValueError:
        raise Exception("Bad MQTT port")
    mqtt_auth = {'username': mqtt_username,
                 'password': mqtt_password}
    mqtt_root_topic = os.environ['ROOT_TOPIC']
    name = os.environ['MQTT_NAME']
    mqtt_topic = "{}/climate/{}/permit".format(mqtt_root_topic, name.lower())
    payload = 30
    publish.single(mqtt_topic,
                   payload=payload,
                   hostname=mqtt_host,
                   port=mqtt_port,
                   auth=mqtt_auth,
                   qos=0,
                   retain=False,
                   client_id="",
                   keepalive=60,
                   will=None,
                   tls=None,
                   protocol=mqtt.MQTTv311,
                   transport="tcp")


if __name__ == '__main__':
    permit()
