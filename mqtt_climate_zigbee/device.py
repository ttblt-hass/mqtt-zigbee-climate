"""Mqtt Zigbee Climate module."""
import asyncio
import logging
import os
import json
from queue import Queue, Empty
import subprocess
import sys
from threading import Thread
import time

import bellows
from bellows.cli import util
import bellows.ezsp
import bellows.zigbee.application
import zigpy.exceptions
import mqtt_hass_base

MAIN_LOOP_WAIT_TIME = 120

ST218_DATA = {'local_temp': 0,
              'pi_heating_demand': 8,
              'occupied_heating_setpoint': 18,
              'setpoint_mode_manuf_specific': 28}

OPERATIONS = {0: 'off',
              4: 'heat',
              5: 'dry'}


CLIMATE_CONFIG_DICT = {
    "name": None,
    "unique_id": None,
    "availability_topic": None,
    "payload_available": "online",
    "payload_not_available": "offline",
    "qos": 0,
    "current_temperature_topic": "homeassistant/zigbee/climate/CLIMATEID/temperature/current",
    "mode_command_topic": "homeassistant/zigbee/climate/CLIMATEID/mode/set",
    "mode_state_topic": "homeassistant/zigbee/climate/CLIMATEID/mode/state",
    "modes": ["off", "heat"],
    "temperature_command_topic": "homeassistant/zigbee/climate/CLIMATEID/temperature/set",
    "temperature_state_topic": "homeassistant/zigbee/climate/CLIMATEID/temperature/state",
    #"away_mode_command_topic": "homeassistant/zigbee/climate/CLIMATEID/away/set",
    #"away_mode_state_topic: "homeassistant/zigbee/climate/CLIMATEID/away/state",
    "min_temp": "5",
    "max_temp": "30",
    "temp_step": "0.5",
    "device": {
        "identifiers": None,
        "manufacturer": None,
        "model": None,
        "sw_version": None,
        }
    }

SENSOR_CONFIG_DICT = {
    "name": None,
    "state_topic": None,
    "unit_of_measurement": "%",
    }


class ZTimer(Thread):
    """Small thread which collect data each 30 seconds."""

    def __init__(self, mqtt_root_topic, devices, command_q, logger):
        """Ztimer init method."""
        Thread.__init__(self)
        self.logger = logger
        self.must_run = False
        self.mqtt_root_topic = mqtt_root_topic
        self.devices = devices
        self.command_q = command_q

    def run(self):
        """Thread's loop."""
        self.logger.info("Starting ZTimer Thread")
        self.must_run = True
        while self.must_run:
            for ieee in self.devices.keys():
                self.logger.debug("Get data for device: %s", str(ieee))
                self.command_q.put({'type': 'read',
                                    'ieee': ieee,
                                    'endpoint_id': 25,
                                    'cluster_id': 513,
                                    'attributes': list(ST218_DATA.values())})
            self.logger.debug("Waiting for %s seconds", MAIN_LOOP_WAIT_TIME)
            wait = 0
            while wait < MAIN_LOOP_WAIT_TIME and self.must_run:
                wait += 1
                time.sleep(2)
        self.logger.info("Terminating ZTimer Thread")


class MqttZigbeeClimate(mqtt_hass_base.MqttDevice):
    """MQTT Zigbee Climate switch."""

    def __init__(self):
        """Constructor."""
        mqtt_hass_base.MqttDevice.__init__(self)
        # Init attributes
        self.command_q = Queue()

        self.mqtt_client = None
        self.app = None

        self.last_queries_time = {}
        self.ztimer = None

    def read_config(self):
        """Read env vars."""
        self.device = os.environ['DEVICE']
        self.baudrate = os.environ['BAUDRATE']
        self.database = os.environ['DATABASE']
        self.channel = os.environ.get('CHANNEL')
        self.bellows = os.environ.get('BELLOWS', 'bellows')

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on connect callback."""

    def _on_publish(self, client, userdata, mid):
        """MQTT on publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):  # pylint: disable=W0613
        """Subscribe to all needed MQTT topic."""
        mqtt_base_topic = "{}/climate/{}".format(self.mqtt_root_topic, self.name.lower())
        topic = "{}/permit".format(mqtt_base_topic)
        client.subscribe(topic)
        self.logger.debug("MQTT subscribing to: %s", topic)
        # Subscribes to device's topics
        for device in self.list_devices():
            topic = "{}/{}/mode/set".format(mqtt_base_topic, device)
            client.subscribe(topic)
            self.logger.debug("MQTT subscribing to: %s", topic)
            topic = "{}/{}/temperature/set".format(mqtt_base_topic, device)
            client.subscribe(topic)
            self.logger.debug("MQTT subscribing to: %s", topic)

    def _on_message(self, client, userdata, msg):  # pylint: disable=W0613
        mqtt_base_topic = "{}/climate/{}".format(self.mqtt_root_topic, self.name.lower())
        try:
            if not msg.topic.startswith(mqtt_base_topic):
                self.logger.warning("Bad topic: %s, msg.topic")
                return

            if msg.topic == "{}/permit".format(mqtt_base_topic):
                self.logger.warning("Permit new device to join the network")
                self.command_q.put({'type': 'permit',
                                    'duration': int(msg.payload)})
            elif msg.topic.startswith("/".join((self.mqtt_root_topic, "climate"))):
                # Stardard endpoint and cluster ids for climate
                endpoint_id = 25
                cluster_id = 513
                _, _, _, str_ieee, command = msg.topic.split("/", 4)
                ieee = util.ZigbeeNodeParamType()(str_ieee)
                device = self.app.devices.get(ieee)
                if device is None:
                    self.logger.error('Device %s not found', str_ieee)
                    return
                if command == "temperature/set":
                    # Set target temperature
                    value = "{:0.0f}".format(float(msg.payload) * 100)
                    attribute = 18
                    self.command_q.put({'type': 'write',
                                        'ieee': ieee,
                                        'endpoint_id': endpoint_id,
                                        'cluster_id': cluster_id,
                                        'attribute': attribute,
                                        'value': value})
                elif command == "mode/set":
                    # Set climate mode
                    value = msg.payload.decode('utf-8')
                    if value == 'off':
                        value = 0
                    elif value == 'heat':
                        value = 4
                    else:
                        self.logger.error('Bad mode for device %s: %s', str_ieee, value)
                    attribute = 28
                    self.command_q.put({'type': 'write',
                                        'ieee': ieee,
                                        'endpoint_id': endpoint_id,
                                        'cluster_id': cluster_id,
                                        'attribute': attribute,
                                        'value': value})
                # For Update
                self.logger.info('Zigbee command sent, force update.')
                self.command_q.put({'type': 'read',
                                    'ieee': ieee,
                                    'endpoint_id': 25,
                                    'cluster_id': 513,
                                    'attributes': list(ST218_DATA.values())})

            else:
                self.logger.warning("Bad topic: %s, msg.topic")
                return
        except BaseException as exp:
            self.logger.error("Error on message: %s", exp)

    def _signal_handler(self, signal_, frame):  # pylint: disable=W0613
        """Handle SIGKILL."""
        self.ztimer.must_run = False
        if self.mqtt_client:
            self.mqtt_client.loop_stop()

    async def send_config(self, device):
        mqtt_base_topic = "{}/climate/{}/{}".format(self.mqtt_root_topic,
                                                    self.name.lower(),
                                                    str(device.ieee).replace(":", ""))
        # send heat demand sensor configuration
        sensor_config = SENSOR_CONFIG_DICT.copy()
        sensor_config["name"] =  "heating-demand-{}".format(str(device.ieee).replace(":", ""))
        sensor_config["state_topic"] = "{}/heating-demand".format(mqtt_base_topic)
        mqtt_config_topic = "{}/sensor/{}/{}-heatingdemand/config".format(self.mqtt_root_topic,
                                                                          self.name.lower(),
                                                                          str(device.ieee).replace(":", ""))
        self.logger.info("New sensor config: %s, %s", mqtt_config_topic, sensor_config)
        self.mqtt_client.publish(topic=mqtt_config_topic,
                                 payload=json.dumps(sensor_config))
        # Send climate configuration
        config = CLIMATE_CONFIG_DICT.copy()
        remote_attr = {"sw_version": None,
                       "manufacturer": None,
                       "model": None,}
        remote_attrs = {'sw_version': 1, 'manufacturer': 4, 'model': 5}
        for name, attr_id in remote_attrs.items():
            tries = 0
            ret = None
            while tries < 3 and ret is None:
                try:
                    ret = await self.read_attributes(device.ieee, 25, 0, [attr_id])
                except zigpy.exceptions.DeliveryError:
                    tries += 1
                    await asyncio.sleep(1)
            if ret is None:
                raise
            elif isinstance(ret[attr_id], int):
                config['device'][name] = ret[attr_id]
            elif isinstance(ret[attr_id], bytes):
                config['device'][name] = ret[attr_id].split(b'\x00')[0].decode('utf-8')
            else:
                raise

        config['name'] = str(device.ieee)
        config['unique_id'] = str(device.ieee)
        config['availability_topic'] = "{}/availability".format(mqtt_base_topic)
        config['current_temperature_topic'] = "{}/temperature/current".format(mqtt_base_topic)
        config['mode_command_topic'] = "{}/mode/set".format(mqtt_base_topic)
        config['mode_state_topic'] = "{}/mode/state".format(mqtt_base_topic)
        config['temperature_command_topic'] = "{}/temperature/set".format(mqtt_base_topic)
        config['temperature_state_topic'] = "{}/temperature/state".format(mqtt_base_topic)
        config['device']['identifiers'] = str(device.ieee)
        mqtt_config_topic = mqtt_base_topic + "/config"
        self.logger.info("New sensor config: %s, %s", mqtt_config_topic, config)
        self.mqtt_client.publish(topic=mqtt_config_topic,
                                 payload=json.dumps(config))

    def list_devices(self):
        """List registered devices."""
        ezsp = bellows.ezsp.EZSP()
        app = bellows.zigbee.application.ControllerApplication(ezsp, self.database)
        return app.devices

    async def read_attributes(self, node, endpoint_id, cluster_id, attributes, manufacturer=None):
        """Read Zigbee attributes."""
        self.logger.debug("Get endpoint and cluster.")
        _, _, cluster = util.get_in_cluster(self.app, node, endpoint_id, cluster_id)

        self.logger.debug("Read cluster attributes.")
        values = await cluster.read_attributes(attributes,
                                               allow_cache=False,
                                               manufacturer=manufacturer)
        self.logger.debug("Cluster attributes read.")
        if not values:
            self.logger.error("Received empty response")
            return {}
        for attribute in attributes:
            if attribute not in values[0]:
                self.logger.error("Attribute %s not successful. Status=%s",
                                  attribute, values[1][attribute])
        return values[0]

    def wait_for_query(self, str_ieee):
        """Wait before running a query to avoir to flood the device.

        Arbitrary choice: 10 seconds.
        """
        if time.time() - self.last_queries_time.get(str_ieee, 0) < 10:
            self.logger.warning("Waiting some time before requesting: %s", str_ieee)
            time.sleep(time.time() - self.last_queries_time.get(str_ieee))

    async def _init_main_loop(self):
        """Init before starting main loop."""
        app_startup = True
        # Check if the network is already formed
        new_network = False
        if not os.path.exists(self.database):
            new_network = True
            app_startup = False
            open(self.database, 'a').close()
        # Setup application
        self.logger.debug("Creating Zigbee application")
        self.app = await util.setup_application(
                self.device,
                self.baudrate,
                self.database,
                startup=app_startup,
            )
        self.logger.debug("Zigbee application created")
        # Form network if database file doesn't exist
        if new_network:
            self.logger.info("Forming a new network on channel %s", self.channel)
            await self.app.initialize()
            self.logger.info("Form a new network on channel %s", self.channel)
            await self.app.form_network(self.channel, pan_id=None, extended_pan_id=None)
            self.logger.info("New network formed on channel %s", self.channel)
        else:
            self.logger.debug("Network created")
        # Ztimer
        ztime_logger = self.logger.getChild("timer")
        ztime_logger.setLevel(getattr(logging, self._loglevel))
        self.ztimer = ZTimer(self.mqtt_root_topic,
                             self.list_devices(),
                             self.command_q,
                             ztime_logger)
        self.ztimer.start()

        for device in self.list_devices().values():
            await self.send_config(device)

    async def _main_loop(self):
        """Run main loop."""
        self.logger.debug("Waiting for command")
        try:
            command = self.command_q.get(block=True, timeout=10)
        except Empty:
            self.logger.debug("Empty")
            return
        self.logger.debug("Command received: %s", command)
        with open('/tmp/last_query', 'w') as fhlq:
            fhlq.write("{:d}".format(int(time.time())))
        try:
            if command['type'] == 'permit':
                # Permit network join and quit
                # TODO get bellows abslute path
                cmd = [self.bellows,
                       "-d",
                       self.device,
                       "permit",
                       "-t",
                       str(command.get('duration', 30)),
                       "-D",
                       self.database]
                subprocess.run(cmd)
                self.must_run = False
                self.ztimer.must_run = False
                if self.mqtt_client:
                    self.mqtt_client.loop_stop()
                # TODO find a way to avoid this restart
                self.logger.warning('New devices detected, exiting, restart needed ...')
                sys.exit(1)
            str_ieee = str(command['ieee'])
            self.wait_for_query(str_ieee)
            self.last_queries_time[str_ieee] = time.time()
            if command['type'] == 'read':
                # Read Zigbee attribute command
                try:
                    base_topic = "/".join((self.mqtt_root_topic, "climate", self.name, str_ieee))
                    values = await self.read_attributes(command['ieee'],
                                                        command['endpoint_id'],
                                                        command['cluster_id'],
                                                        command['attributes'])
                    self.logger.debug("Got: %s => %s", command['ieee'], values)
                    local_temp = "{:0.2f}".format(values[0] / 100)
                    self.mqtt_client.publish(topic=base_topic + "/temperature/current",
                                             payload=local_temp)
                    heating_demand = "{:d}".format(values[8])
                    self.mqtt_client.publish(topic=base_topic + "/heating-demand",
                                             payload=heating_demand)
                    if values[18] > 0:
                        target_temp = "{:0.2f}".format(values[18] / 100)
                        self.mqtt_client.publish(topic=base_topic + "/temperature/state",
                                                 payload=target_temp)
                    current_mode = OPERATIONS[values[28]]
                    self.mqtt_client.publish(topic=base_topic + "/mode/state",
                                             payload=current_mode)
                    self.mqtt_client.publish(topic=base_topic + "/availability",
                                             payload="online")
                except BaseException as exp:
                    self.logger.error("Error fetch device %s: %s", str_ieee, exp)
                    # TODO not sure this is useful
                    self.mqtt_client.publish(topic=base_topic + "/availability",
                                             payload="offline")
                    return
            elif command['type'] == 'write':
                # Write Zigbee attribute command
                _, _, cluster = util.get_in_cluster(self.app,
                                                    command['ieee'],
                                                    command['endpoint_id'],
                                                    command['cluster_id'])
                try:
                    await cluster.write_attributes({command['attribute']: command['value']},
                                                   manufacturer=command.get('manufacturer'))
                    self.logger.info("Command done: %s", command)
                except BaseException:
                    self.logger.error("Error write attribute: %s", command)
                    # TODO reput the command in the queue
                    # self.command_q.put(command, block=True, timeout=5)
                    return
            else:
                self.logger.error("Bad command type received: %s", command)
                return
            # Small wait to avoid to overload the USB device
            await asyncio.sleep(2)
        except BaseException as exp:
            self.logger.error("Error in main loop: %s", exp)

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
        self.mqtt_client.loop_stop()
        self.ztimer.must_run = False
