# MQTT ZigBee Server

This small daemon/docker image can control a StelPro Heater:
- ST-218

It can be use with Home-assistant

## Home-assistant configuration

```
climate:
  - platform: mqtt
    name: st218
    unique_id: "f8:f0:05:ff:ff:d0:ad:70"
    availability_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/avaibility"
    payload_available: "online"
    payload_not_available: "offline"
    current_temperature_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/temperature/current"
    mode_command_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/mode/set"
    mode_state_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/mode/state"
    modes:
    - "off"
    - "heat"
    temperature_command_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/temperature/set"
    temperature_state_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/temperature/state"
    #away_mode_command_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/away/set"
    #away_mode_state_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/away/state"
    min_temp: 5
    max_temp: 30
    temp_step: 0.5
    device:
      identifiers: "f8:f0:05:ff:ff:d0:ad:70"
      manufacturer: Stelpro
      model: ST218
      sw_version: "106"

sensor:
  - platform: mqtt
    state_topic: "hass/zigbee/climate/f8:f0:05:ff:ff:d0:ad:70/heating-demand"
    name: st218-heating-demand
    unit_of_measurement: "%"
```

## Run it locally

Run it:
```
pip install -r requirements.txt

sudo MQTT_USERNAME=hass \
	MQTT_PASSWORD=hass \
	MQTT_HOST=192.168.3.1 \
	MQTT_PORT=1883 \
	ROOT_TOPIC=homeassistant \
	MQTT_NAME=climate-zigbee \
	LOG_LEVEL=DEBUG DEVICE=/dev/ttyUSB1 \
	BAUDRATE=57600 \
	CHANNEL=15 \
	DATABASE=app.db \
	BELLOWS=/home/tcohen/perso/gits/gitlab.com/titilambert/mqtt-zigbee-climate/env/bin/bellows \
	env/bin/mqtt_climate_zigbee
```

Permit
```
MQTT_USERNAME=hass \
	MQTT_PASSWORD=hass \
	MQTT_HOST=192.168.3.1 \
	MQTT_PORT=1883 \
	ROOT_TOPIC=homeassistant \
	MQTT_NAME=climate-zigbee \
	LOG_LEVEL=DEBUG \
	ROOT_TOPIC=homeassistant \
	env/bin/mqtt_climate_zigbee_permit 
```

## Build Docker image

```
docker build -t mqttzigbeeclimate .
```

## Run Docker container

```
docker run --rm -it \
    --name bttest \
    --privileged --net=host \
	-e DEVICE=/dev/ttyUSB1 \
    -e DATABASE=/data/app.db \
    -e BAUDRATE=57600 \
    -e MQTT_USERNAME=hass \
    -e MQTT_PASSWORD=hass \
    -e MQTT_HOST=192.168.0.1 \
    -e MQTT_PORT=1883 \
    -e ROOT_TOPIC=hass/zigbee \
    -e CHANNEL=15 \
    -e LOG_LEVEL=INFO \
    -v `pwd`/data:/data \
    mqttzigbeeclimate:latest bash
```


