# Notes

## Links

* https://github.com/titilambert/home-assistant/blob/st218/homeassistant/components/climate/zha.py
* http://pro.stelpro.com/contenu/ca/pdf/guides/User/SOR.pdf
* https://community.matrix.one/t/first-zigbee-tests-with-python-bellows-forming-a-network-controller/1579
* https://www.zigbee.org/zigbee-products-2/#zigbeecertifiedproducts/productdetails3/576aefccf87038aa313e32f1/
* https://www.zigbee.org/zigbee-products-2/#zigbeecertifiedproducts/productdetails3/576aefccf87038aa313e32f1/kn-asset/26-31-31-576c14c3314c73bd3aa6151c/picsst218_2.xml


# Install/test bellows

## Prepare python env

```
virtualenv -p `which python3` env
pip install https://github.com/zigpy/bellows/archive/master.zip
```

## Scan Zigbee devices

```
sudo env/bin/bellows -d /dev/ttyUSB1 scan
```

## Create new Zigbeenetwork

```
touch app.db
sudo env/bin/bellows -d /dev/ttyUSB1 form -D app.db -c 15
```

## Let device join the network

```
sudo env/bin/bellows -d /dev/ttyUSB1 permit -D app.db 
```

## List devices connected

```
sudo env/bin/bellows -d /dev/ttyUSB1 devices -D app.db 
```

## Tested attributes

### LOCAL_TEMPERATURE => current temperature

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 0
1850
```

### THERMOSTAT_OCCUPANCY ????

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 2
1
```

### ABS_MIN_HEAT_SETPOINT_LIMIT ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 3
500
```

### ABS_MAX_HEAT_SETPOINT_LIMIT ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 4
3000
```

### ABS_MIN_COOL_SETPOINT_LIMIT ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 5
700
```

### ABS_MAX_COOL_SETPOINT_LIMIT ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 6
4000
```

### PI_HEATING_DEMAND => current heat power

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 8
```

### HVAC_SYSTEM_TYPE_CONFIGURATION ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 9
0
```

### LOCAL_TEMPERATURE_CALIBRATION ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 16
0
```

### OCCUPIED_COOLING_SETPOINT  ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 17
3500
```

### OCCUPIED_HEATING_SETPOINT ==> wanted temperate

* Write:
```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 write-attribute 18 18
```

* Read:
```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 18
1800
```

### UNOCCUPIED_COOLING_SETPOINT  ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 19
4000
```

### UNOCCUPIED_HEATING_SETPOINT ==> wanted temperature in away mode

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 20
1700
```

### MIN_HEAT_SETPOINT_LIMIT ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 21
500
```

### MAX_HEAT_SETPOINT_LIMIT ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 22
3000
```

### MIN_COOL_SETPOINT_LIMIT ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 23
700
```

### MAX_COOL_SETPOINT_LIMIT ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 24
4
```

### MIN_SETPOINT_DEAD_BAND ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 25
25
```

### REMOTE_SENSING ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 26
2
```

### CONTROL_SEQUENCE_OF_OPERATION ???

```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 27
2
```

### SYSTEM_MODE: Current mode

* 0: 'off'
* 4: 'confort'
* 5: 'eco' ==> away in home assistant


Write:
```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 write-attribute 28 0
```

* Read:
```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 28
4
```

### SETPOINT_MODE_MANUF_SPECIFIC: Eco mode

* 0: 'off'
* 4: 'confort'
* 5: 'eco' ==> away in home assistant

Notes from the SOR dev:
```
Afin de pouvoir mettre le SOR en mode ECO, vous devez envoyer la valeur 0x04 à l’attribut 0x001C afin de placer l’appareil en mode « Heat »

Ensuite, vous devez envoyer la valeur 0x05 à l’attribut 0x401c afin de placer l’appareil en mode « Eco »

Ceci est nécessaire puisque le mode Eco est un mode « custom » de Stelpro.

Dans les modes zigbee standard, il n’y a que 0x04 qui correspond au mode de chauffage.

Par soucis de compatibilité, nous avons décidé d’ajouter un attribut custom (0x401c) qui permet le mode Eco.
```

Write to eco mode
```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 write-attribute 28 4
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 write-attribute 16412 5
```

Read
```
sudo env/bin/bellows -d /dev/ttyUSB1 zcl -D app.db f8:f0:05:ff:ff:d0:ad:70 25 513 read-attribute 16412
4
```

## Leave the network

```
sudo env/bin/bellows -d /dev/ttyUSB1 leave
```
